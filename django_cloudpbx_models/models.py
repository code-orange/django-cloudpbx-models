from django.db import models


class CusIvr(models.Model):
    ivr_name = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = "cus_ivr"


class CusIvrApps(models.Model):
    name = models.CharField(max_length=60)

    class Meta:
        managed = False
        db_table = "cus_ivr_apps"


class CusIvrTasks(models.Model):
    ivr = models.ForeignKey(CusIvr, models.DO_NOTHING)
    application = models.ForeignKey(
        CusIvrApps, models.DO_NOTHING, db_column="application"
    )
    value = models.CharField(max_length=60, blank=True, null=True)
    task_order = models.IntegerField()

    class Meta:
        managed = False
        db_table = "cus_ivr_tasks"
        unique_together = (("ivr", "task_order"),)
